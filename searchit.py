#! /usr/bin/env python

import tornado.ioloop
import tornado.web

PORT = 9999
song_list = []

class InputHandler(tornado.web.RequestHandler):
    def get(self):
        song = self.get_argument('song', None)
        
        if song in song_list:
            self.write('<b>{}</b>'.format(song))
            
        self.write('''
 <h1>Enter A SONG</h1>
 <form>
     <input type="text" song="song" value="{}">
     <input type="submit" value="Echo!">
 </form>
 '''.format(name))
        
Application = tornado.web.Application([
    (r"/", InputHandler),
], debug=True)

Application.listen(PORT)
tornado.ioloop.IOLoop.current().start()

if __name__ == "__main__":
    app = make_app()
    app.listen(9999)
    tornado.ioloop.IOLoop.current().start()