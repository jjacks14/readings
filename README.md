CDT 30020 Script-based Programming II (Spring 2016) - Readings
==============================================================

- **NetID**: jjacks14

## Readings

1. [Reading 00](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading00.html)

2. [Reading 01](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading01.html)

3. [Reading 02](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading02.html)

4. [Reading 03](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading03.html)

5. [Reading 04](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading04.html)

6. [Reading 05](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading05.html)

7. [Reading 06](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading06.html)

8. [Reading 07](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading07.html)

9. [Reading 08](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading08.html)

10. [Reading 09](https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/reading09.html)
