====================

**Score**:3/5

Deductions
‐‐‐‐‐‐‐‐‐‐
3.) -1
4.) -1

Comments
‐‐‐‐‐‐‐‐
3.) A Raspberry Pi is not an operating system. It is actually a small computer and differs from your laptop in terms of size and form factore, but is a computer nonetheless.
4.) Ubuntu Mate is an operating system. Raspberry Pi needs Ubuntu Mate because hardware needs software.