Reading 04
==========

## Responses
1a. Database:a file organized for storing data that is in permanent storage
1b. Table: a component in the database that consist of rows and columns
1c. Column: given a name(address) and a column type (Integer, Date, or Text)
1d. Row: actual data in table 

2. A SQL is structured query language. It is the standard language for 
databases. 
2a. SELECT: pick specific information in database
2b. INSERT: Add infomation in database 
2c. UPDATE: specify database information to change 
2d. DELETE: remove information in database 

 
## Feedback

## Grading

