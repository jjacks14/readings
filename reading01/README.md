Reading 01
==========

## Responses
(1) A script is a file with instructions that are executed when the script is called. A module is a good way to organize code. 
A package consist of submodules. A python file can be both because if a function calls several scripts it should be put 
in a module. 



(2) When you import something, you are given access to it's objects. Using import * is a dangerous command, because 
it is impossible to find it's orginal attribute, and simply bad code. It makes it harder to follow. 


## Feedback

## Grading
2
o	Q3-5 missing -3
