Reading 03
==========

## Responses
(1) A URL consist of a protocol, host, port, resource path, and query. In the
url http://127.0.0.1:9123/files?name=echosmith.mp3 the following parts 
correspond to the following components.
		protocol --> http
		host -->127.0.0.1
		port -->9123
		resource path --> files 
		query --> name=echosmith.mp3
(2)The client and host communicate via request/response. The client(computer)
 begins with a request message, and the host responds. 

## Feedback

## Grading
3
Script doesn’t serve a website, .py errors out -2
May need to change the value in the .format

