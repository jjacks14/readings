#!/usr/bin/env python2.7

import logging
import os
import socket
import sqlite3
import sys

import tornado.ioloop
import tornado.options
import tornado.web

# Constants

PORT     = 9998
DB_PATH  = 'teams.db'
CSV_PATH = 'teams.csv'

# Database

class Database(object):
    def __init__(self):
        # TODO: Connect to database
        self.conn = sqlite3.connect(DB_PATH)

        # TODO: Create table and load CSV data
        with self.conn:
            curs = self.conn.cursor()
            statement = '''
            CREATE TABLE IF NOT EXISTS Teams(
                name            TEXT NOT NULL, 
                Image           TEXT NOT NULL,
                PRIMARY KEY (name)
            )
            '''

            curs.execute(statement)

            for line in open(CSV_PATH):
                name,image = line.split(',')
                statement = '''
                INSERT OR REPLACE INTO Teams (name,image) VALUES (?, ?)
                '''
                curs.execute(statement, (name, image))
    
    def search(self, query):
        # TODO: Return teams whose Name is similar to query
        with self.conn:
            curs      = self.conn.cursor()
            statement = 'SELECT * FROM Teams WHERE Name LIKE ?'
            return curs.execute(statement, ('%{}%'.format(query),))

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        # TODO: Get query value from form

        query = self.get_argument('query','')

        # TODO: Search for teams if query is valid

        if query != '':
            teams = self.application.database.search(query)

        # TODO: Render 'teams.html' template by passing search results
            self.render('teams.html', teams=teams, query=query)
        else:
            teams = ''
            self.render('teams.html', teams=teams, query=query)

# Application

class Application(tornado.web.Application):
    def __init__(self, port=PORT):
        tornado.web.Application.__init__(self, debug=True)
        self.logger   = logging.getLogger()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        # TODO: Define database and port instance variables

        self.database=Database()
        self.port = port

        # TODO: Add IndexHandler

        self.add_handlers('', (
            (r'/'           , IndexHandler),

            ))

    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Main Execution

if __name__ == '__main__':
    tornado.options.parse_command_line()

    application = Application()
    application.run()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
